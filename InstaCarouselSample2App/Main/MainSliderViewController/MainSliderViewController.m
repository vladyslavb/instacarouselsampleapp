//
//  MainSliderViewController.m
//  InstaCarouselSample2App
//
//  Created by Vladyslav Bedro on 8/28/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainSliderViewController.h"

//classes
#import "DKCarouselView.h"

@interface MainSliderViewController () <UIGestureRecognizerDelegate>

//outlets
@property (weak, nonatomic) IBOutlet DKCarouselView* carouselView;

//properties
@property (assign, nonatomic) CGFloat globalViewScale;

@end

@implementation MainSliderViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureMainSlider];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) configureMainSlider
{
    NSArray* images = @[@"https://cdn.tourradar.com/s3/tour/original/79329_51048c99.jpg",
                        @"https://www.roughguides.com/wp-content/uploads/2012/11/SIM-452542-660x420.jpg",
                        @"https://handluggageonly.co.uk/wp-content/uploads/2018/02/Hand-Luggage-Only-8-5.jpg",
                        @"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDFomojX5AlRuTauLjSB1MeHTlMdCd9kaKnLLeNUiyqT0lKUsU",
                        @"http://www.travelplaces24x7.com/wp-content/uploads/2017/06/Best-Time-to-visit-Bogliasco-Italy.jpg",
                        ];
    
    NSMutableArray* items = [NSMutableArray new];
    
    for (NSString* imageUrl in images)
    {
        DKCarouselURLItem* urlAD = [DKCarouselURLItem new];
        
        urlAD.imageUrl = imageUrl;
        
        [items addObject: urlAD];
    }
    
    self.carouselView.defaultImage = [UIImage imageNamed: @"defaultImage"];
    
    [self.carouselView setIndicatorTintColor: [UIColor redColor]];
    
    [self.carouselView setItems: items];
    
    [self.carouselView setDidSelectBlock: ^(DKCarouselItem* item, NSInteger index) {
        NSLog(@"%zd",index);
    }];
    
    [self.carouselView setDidChangeBlock: ^(DKCarouselView* view, NSInteger index) {
        NSLog(@"%@, %zd", view, index);
    }];
    
    UIPinchGestureRecognizer* pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget: self
                                                                                       action: @selector(handlePinch:)];
    pinchGesture.delegate = self;
    
    [self.view addGestureRecognizer: pinchGesture];
}


#pragma mark - Actions for gestures -

- (void) handlePinch: (UIPinchGestureRecognizer*) pinchGesture
{
    CGFloat newScale = 1.f + pinchGesture.scale - self.globalViewScale;
    
    CGAffineTransform currentTransform = self.carouselView.transform;
    
    CGAffineTransform   newTransform = CGAffineTransformScale(currentTransform, newScale, newScale);
    
    self.carouselView.transform = newTransform;
    
    self.globalViewScale = pinchGesture.scale;
    
    if (pinchGesture.state == UIGestureRecognizerStateEnded  ||
        pinchGesture.state == UIGestureRecognizerStateFailed ||
        pinchGesture.state == UIGestureRecognizerStateCancelled
        )
    {
        CGAffineTransform newTransform = CGAffineTransformMakeScale(1.f, 1.f);
        
        self.carouselView.transform = newTransform;
    }
}


#pragma mark - Actions -

- (IBAction) onSwitchValueChanged: (UISwitch*) sender
{
    if (sender.on)
    {
        [self.carouselView setAutoPagingForInterval: 5];
    }
    else
    {
        [self.carouselView setPause: YES];
    }
}


#pragma mark UIGestureRecognizerDelegate

- (BOOL)                         gestureRecognizer: (UIGestureRecognizer*) gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer: (UIGestureRecognizer*) otherGestureRecognizer
{
    return YES;
}


@end




























